<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ConferenceCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string',
            'description' => 'required|string',
            'places' => 'required|numeric',
            'date' => 'required|date',
            'start_at' => 'required|date_format:H:i',
            'end_at' => 'required|date_format:H:i',
            'limit_hours_to_enroll' => 'required|numeric',
            'places' => 'required|numeric'
        ];
    }
}
