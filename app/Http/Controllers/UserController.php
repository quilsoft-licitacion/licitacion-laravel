<?php

namespace App\Http\Controllers;

use App\Services\UserService;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function myEnrollments(UserService $service)
    {
        $enrollments = $service->getNextEnrollments();
        return view('user.next-enrollments')->with('enrollments', $enrollments);
    }

    public function enrollmentsHistory(UserService $service)
    {
        $enrollments = $service->getEnrollmentsHistory();
        return view('user.history-enrollments')->with('enrollments', $enrollments);
    }
}
