<?php

namespace App\Http\Controllers;

use App\Services\EnrollmentService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class EnrollmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, EnrollmentService $service)
    {
        try {
            $service->enroll($request->conference_id);
            return Redirect::action('ConferenceController@nextConferences')->with('success', 'Inscripcion completa');
        } catch (Exception $e) {
            return Redirect::back()->with('error', $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function enrollment(EnrollmentService $service, $conference_id)
    {
        $enrollment = $service->getEnrollment($conference_id);
        if ($enrollment->user_id != Auth::id()) {
            return Redirect::back()->with('error', 'No authorizado');
        }
        return view('enrollment.show')->with('enrollment', $enrollment);
    }

    public function cancelEnrollment(EnrollmentService $service, $conference_id)
    {
        try {
            $service->cancelEnrollment($conference_id);
            return Redirect::action('ConferenceController@nextConferences');
        } catch (Exception $e) {
            return Redirect::back()->with('error', $e->getMessage());
        }
    }

    public function deleteEnrollment(Request $request, EnrollmentService $service, $enrollment_id)
    {
        $service->deleteEnrollment($enrollment_id);
        return Redirect::action('ConferenceController@index')->with('success', 'Inscripcion eliminada correctamente');
    }
}
