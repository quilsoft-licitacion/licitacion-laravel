<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ConferenceCreateRequest;
use App\Http\Requests\ConferenceUpdateRequest;
use App\Services\ConferenceService;
use Exception;
use Illuminate\Support\Facades\Redirect;

class ConferenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ConferenceService $service)
    {
        return view('conferencias.admin')->with('conferences', $service->all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('conferencias.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ConferenceCreateRequest $request, ConferenceService $service)
    {
        try {

            $service->save($request->all());
            return Redirect::route('conferencias.index');
        } catch (Exception $e) {
            return Redirect::back()->with('error', $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ConferenceService $service, $id)
    {
        $conference = $service->find($id);
        return view('conferencias.edit')->with('conference', $conference);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(ConferenceService $service, $id)
    {
        $conference = $service->find($id);
        return view('conferencias.edit')->with('conference', $conference);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ConferenceService $service, $id)
    {
        try {
            $input = $request->all();
            $service->update($id, $input);
            return Redirect::route('conferencias.index')->with('success', 'Conferencia editada con exito');
        } catch (Exception $e) {
            return Redirect::back()->with('error', 'asdasd');
            //return Redirect::back()->with('error', $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(ConferenceService $service, $id)
    {
        try {

            $service->delete($id);
            return Redirect::route('conferencias.index')->with('success', 'Conferencia eliminada con exito');
        } catch (Exception $e) {
            return Redirect::back()->with('error', $e->getMessage());
        }
    }

    public function nextConferences(ConferenceService $service)
    {
        $conferences = $service->nextConferences();

        return view('conferencias.next')->with('conferences', $conferences);
    }

    public function showConference(ConferenceService $service, $id)
    {
        $conference = $service->find($id);
        return view('conferencias.showToStudent')->with('conference', $conference);
    }

    public function list(ConferenceService $service, $conference_id)
    {
        $enrollments = $service->getEnrollments($conference_id);
        return view('conferencias.enrollments')->with('enrollments', $enrollments);
    }
}
