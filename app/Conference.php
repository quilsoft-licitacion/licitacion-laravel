<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Conference extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'id',
        'name',
        'description',
        'date',
        'start_at',
        'end_at',
        'limit_hours_to_enroll',
        'places'
    ];
    public function enrollments()
    {
        return $this->hasMany('App\Enrollment');
    }

    public function placesAvailable()
    {
        $takenPlaces = $this->enrollments->count();
        return $this->places - $takenPlaces;
    }
}
