<?php

namespace App\Services;

use App\Enrollment;
use Exception;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class EnrollmentService
{
    private $enrollment;

    public function __construct(Enrollment $enrollment)
    {
        $this->enrollment = $enrollment;
    }

    public function getEnrollment($id)
    {
        return $this->enrollment
            ->where('id', $id)
            ->with('conference')
            ->first();
    }

    public function enroll($conference_id)
    {

        $user = Auth::user();
        $conference = \App\Conference::find($conference_id);

        $now = Carbon::now();
        $limit_to_enroll = new Carbon($conference->date);
        $limit_to_enroll->startOfDay();
        $conference_time = new Carbon($conference->start_at);
        $limit_to_enroll->setHours($conference_time->hour);
        $limit_to_enroll->setMinutes($conference_time->minute);
        $limit_to_enroll->subHours($conference->limit_minutes_to_enroll);

        $enrollments = $this->enrollment
            ->where('conference_id', $conference_id)
            ->count();

        /* Validacion cupo */
        if ($enrollments >= $conference->places) {
            throw new Exception('No hay cupos disponibles');
        }

        /* Validacion fecha y hora */
        if ($now->greaterThanOrEqualTo($limit_to_enroll)) {
            throw new Exception('Las inscripciones a este evento han terminado');
        }

        $is_already_enrolled = $this->enrollment
            ->where('conference_id', $conference_id)
            ->where('user_id', $user->id)
            ->count() != 0;

        /* Valido si el estudiante ya esta inscripto */
        if ($is_already_enrolled) {
            throw new Exception('El usuario ya se encuentra inscripto');
        }

        /* Inscripcion a la conferencia */
        $enrollment = new Enrollment();
        $enrollment->user_id = $user->id;
        $enrollment->conference_id = $conference_id;
        $enrollment->save();
    }

    public function cancelEnrollment($conference_id)
    {
        $user = Auth::user();
        $enrollment = $this->enrollment
            ->where('conference_id', $conference_id)
            ->where('user_id', $user->id)
            ->first();
        if ($enrollment != null) {
            $enrollment->delete();
        }
    }

    public function deleteEnrollment($enrollment_id)
    {
        /* $enrollment = $this->enrollment
            ->where('conference_id', $conference_id)
            ->where('user_id', $user_id)
            ->first();
        if ($enrollment != null) {
            $enrollment->delete();
        } */
        $this->enrollment->destroy($enrollment_id);
    }
}
