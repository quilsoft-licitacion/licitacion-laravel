<?php

namespace App\Services;

use App\Conference;
use Exception;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;

class ConferenceService
{

    private $conference;

    public function __construct(Conference $conference)
    {
        $this->conference = $conference;
    }

    public function save(array $request): void
    {
        try {

            $this->conference->create($request);
        } catch (Exception $e) {
            Log::error('Breakpoint ' . $e->getMessage());
        }
    }

    public function update($id, array $request): void
    {
        try {
            $conference = $this->conference->find($id);
            $conference->update($request);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function all()
    {
        return $this->conference
            ->orderBy('date', 'DESC')
            ->paginate(15);
    }

    public function delete($id): void
    {
        $this->conference->destroy($id);
    }

    public function find($id)
    {
        return $this->conference->find($id);
    }

    public function nextConferences()
    {
        $now = Carbon::now();
        return $this->conference
            ->whereDate('date', '>', $now->toDateString())
            ->orWhere(function ($q) use ($now) {
                $q->whereDate('date', '=', $now->toDateString())
                    ->whereTime('start_at', '>', $now->toTimeString());
            })
            ->paginate(15);
    }

    public function getEnrollments($id)
    {
        return $this->conference
            ->where('id', $id)
            ->first()
            ->enrollments()
            ->paginate(15);
    }
}
