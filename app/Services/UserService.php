<?php

namespace App\Services;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;

class UserService
{
    public function getNextEnrollments()
    {
        $user = Auth::user();
        $now = Carbon::now();
        /* Primero mas proximas */
        return $user->enrollments()
            ->whereHas('conference', function (Builder $query) use ($now) {
                $query->whereDate('date', '>', $now->toDateString())
                    ->orWhere(function ($q) use ($now) {
                        $q->whereDate('date', '=', $now->toDateString())
                            ->whereTime('start_at', '>', $now->toTimeString());
                    })
                    ->orderBy('date', 'ASC')
                    ->orderBy('start_at', 'ASC');
            })
            ->paginate(15);
    }

    public function getEnrollmentsHistory()
    {
        $user = Auth::user();
        $now = Carbon::now();
        /* Primero mas recientes pero terminadas */
        return $user->enrollments()
            ->whereHas('conference', function (Builder $query) use ($now) {
                $query->whereDate('date', '<', $now->toDateString())
                    ->orWhere(function ($q) use ($now) {
                        $q->whereDate('date', '=', $now->toDateString())
                            ->whereTime('start_at', '>', $now->toTimeString());
                    })
                    ->whereTime('start_at', '<=', $now->toTimeString())
                    ->orderBy('date', 'DESC')
                    ->orderBy('start_at', 'DESC');
            })
            ->paginate(15);
    }
}
