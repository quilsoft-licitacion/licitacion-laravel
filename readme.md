## Guia para instalar docker y correr el proyecto

Si ya tienes instalado docker y es un versión vieja utilizar el siguiente comando para  desinstalar.
   sudo apt-get remove docker docker-engine docker.io containerd runc

Para instalar comenzado actualizando los repositorios.
    sudo apt-get update
Después de actualizar nuestro repositorios comenzando la instalación via apt.
    sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common

Agregar la clave oficial de GPG de docker 

$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

Verificar que se instalaron las claves

sudo apt-key fingerprint 0EBFCD88
Para esta parte se debe verificar que versión tienes de sistema operativo si es x86_64/amd64

sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
Actualizar los ficheros del sistema 
sudo apt-get update
Instalar los componentes de docker
sudo apt-get install docker-ce docker-ce-cli containerd.io




Verificar las versiones disponibles en tu repositorio
apt-cache madison docker-ce
ejemplo 
 docker-ce | 5:18.09.1~3-0~ubuntu-xenial | https://download.docker.com/linux/ubuntu  xenial/stable amd64 Packages
  docker-ce | 5:18.09.0~3-0~ubuntu-xenial | https://download.docker.com/linux/ubuntu  xenial/stable amd64 Packages
  docker-ce | 18.06.1~ce~3-0~ubuntu       | https://download.docker.com/linux/ubuntu  xenial/stable amd64 Packages
  docker-ce | 18.06.0~ce~3-0~ubuntu       | https://download.docker.com/linux/ubuntu  xenial/stable amd64 Packages
  …

Despues instalar dependiendo de tu version de sistema operativo ejemplo
$ sudo apt-get install docker-ce=<18.06.0~ce~3-0~ubuntu> docker-ce-cli=<18.06.0~ce~3-0~ubuntu> containerd.io

Verificar que todo salio bien 
sudo docker run hello-world

Es necesario también instalar el docker-composer comenzamos con el siguiente comando 
sudo curl -L "https://github.com/docker/compose/releases/download/1.24.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
Habilitamos los permisos para la instalación 
sudo chmod +x /usr/local/bin/docker-compose
 
Ejemplo
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
levantar y correr
Clonar repositiorio
$ git clone https://gitlab.com/quilsoft-licitacion/licitacion-laravel.git
$ cd licitacion-quilsoft
construir imágenes y levantar servicios
docker-compose build
docker-compose up -d

./composer install 
./php-artisan key:generate
./php-artisan migrate
./php-artisan db:seed 


## scripts de ayuda
ejecucion de composer, php artisan o phpunit con el contenedor php con estos script de ayudas 
## container
ejecutar ./<
$ ./container
root@8cf37a093502:/var/www/html$
## db
Running ./db connects to your database container's daemon using mysql client.
$ ./db
mysql>
## composer
Ejecutar el comando composer, ejemplos
$ ./composer dump-autoload
Generating optimized autoload files> Illuminate\Foundation\ComposerScripts::postAutoloadDump
> @php artisan package:discover --ansi
Discovered Package: beyondcode/laravel-dump-server
Discovered Package: fideloper/proxy
Discovered Package: laravel/tinker
Discovered Package: nesbot/carbon
Discovered Package: nunomaduro/collision
Package manifest generated successfully.
Generated optimized autoload files containing 3527 classes
 
## php-artisan
Ejecutar comandos php artisan, ejemplos
$ ./php-artisan make:controller BlogPostController --resource
php artisan make:controller BlogPostController --resource
Controller created successfully.
 
 
 
phpunit
Ejecute ./vendor/bin/phpunit para ejecutar pruebas, por ejemplo
$ ./phpunit --group=failing
vendor/bin/phpunit --group=failing
PHPUnit 7.5.8 by Sebastian Bergmann and contributors.
Time: 34 ms, Memory: 6.00 MB
No tests executed!
