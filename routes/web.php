<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;

Route::get('/', function () {
    if (!is_null(Auth::user()) && Auth::user()->isAdmin()) {
        return redirect()->action('ConferenceController@index');
    } else {
        return redirect()->action('ConferenceController@nextConferences');
    }
});

Auth::routes();

Route::get('home', function () {
    if (!is_null(Auth::user()) && Auth::user()->isAdmin()) {
        return redirect()->action('ConferenceController@index');
    } else {
        return redirect()->action('ConferenceController@nextConferences');
    }
});
Route::get('create-conference', 'ConferenceController@create');
Route::get('proximas-conferencias', 'ConferenceController@nextConferences');
Route::get('proximas-conferencias/{conference_id}', 'ConferenceController@showConference');

Route::group(['middleware' => 'auth'], function () {
    Route::post('inscribir/{conference_id}', 'EnrollmentController@store');
    Route::post('cancel-enrollment', 'EnrollmentController@store');
    Route::get('historial', 'UserController@enrollmentsHistory');
    Route::get('mis-inscripcion', 'UserController@myEnrollments');
    Route::get('inscripcion/{conference_id}', 'EnrollmentController@enrollment');
});
Route::group(['middleware' => ['auth', 'is_admin']], function () {
    Route::resource('conferencias', 'ConferenceController');
    Route::get('conferencias/{conference_id}/inscripciones', 'ConferenceController@list');
    Route::delete('delete-enrollment/{enrollment_id}', 'EnrollmentController@deleteEnrollment');
});
