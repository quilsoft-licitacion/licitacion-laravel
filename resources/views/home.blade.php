@extends('layouts.app')

@section('content')


<div class="container">
        <h1 style="font-size: 30px;">Conferencias</h1>
        <table class="table table-striped table-bordered">
            <thead>
                <th class="col-md-1">Id</th>
                <th class="col-md-1">Nombre</th>
                <th class="col-md-1">Descripcion</th>
                <th class="col-md-1">Fecha y Hora</th>
                <th class="col-md-1">Fecha Limite</th>
                <th class="col-md-1">Cupon Disponible</th>
                
            </thead>
            <tbody>
            @foreach($conferences as $conference)
                    <tr>
                        <td> {{$conference->id}} </td>
                        <td>
                            
                            {{ $conference->name }}
                        </td>
                        <td>{{ $conference->description }}</td>
                        <td>{{ $conference->date}}</td>
                        <td>{{ $conference->start_at }}</td>
                        <td>
                            {{ $conference->places }}
                            
                        </td>
                        
                        <td>
                            <ul>
                            <li class="button-list">
                                <a href="" class="btn btn-success">Inscribirme</a>
                            </li>
                               
                            </ul>
                        </td>

                    </tr>
                @endforeach
               

            </tbody>
        </table>
        
    </div>    


@endsection
