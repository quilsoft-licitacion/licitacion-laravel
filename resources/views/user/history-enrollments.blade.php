@extends('layouts.app')

@section('content')


<div class="container">
    <h1 style="font-size: 30px;">Conferencias</h1>
    <table class="table table-striped table-bordered">
        <thead>
            <th class="col-md-1">Conferencia</th>
            <th class="col-md-1">Descripcion</th>
            <th class="col-md-1">Fecha</th>
            <th class="col-md-1">Inicio</th>
            <th class="col-md-1">Fin</th>
        </thead>
        <tbody>
            @foreach($enrollments as $enrollment)
            <tr>
                <td>

                    {{ $enrollment->conference->name }}
                </td>
                <td>{{ $enrollment->conference->description }}</td>
                <td>{{ $enrollment->conference->date}}</td>
                <td>{{ $enrollment->conference->start_at }}</td>
                <td>{{ $enrollment->conference->end_at }}</td>
            </tr>
            @endforeach


        </tbody>
    </table>

</div>


@endsection