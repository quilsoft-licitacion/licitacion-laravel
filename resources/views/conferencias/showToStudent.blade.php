@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            @if(Session::has('success'))
            <div class="alert alert-success" role="alert">
                {{Session::get('success')}}
            </div>

            @endif
            @if (\Session::has('error'))
            <div class="alert alert-danger">
                {{session('error')}}
            </div>
            @endif
            <form method="POST" action="{{ url('/inscribir\\'.$conference->id) }}">
                @csrf
                <input type="hidden" name="conference_id" value="{{ $conference->id }}">

                <div class="container">

                    <div class="card" style="text-align:center">

                        <div class="row">
                            <div class="col-12">

                                <div class="card-header">Detalle del evento :</div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12">
                                <p class="text-size-name">{{ $conference->name }}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <p class="text-size">{{ $conference->description }}</p>
                            </div>
                        </div>

                        <div class="row" style="margin-bottom:10px">

                            <div class="col-4 offset-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Inscribirme') }}
                                </button>
                            </div>
                        </div>

                    </div>
                </div>

        </div>

        </form>
    </div>
</div>
</div>
@endsection