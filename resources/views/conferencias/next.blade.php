@extends('layouts.app')

@section('content')



<div class="container">
    @if(Session::has('success'))
    <div class="alert alert-success" role="alert">
        {{Session::get('success')}}
    </div>

    @endif
    @if (\Session::has('error'))
    <div class="alert alert-danger">
        @foreach ($errors->all() as $error)
        {{ $error }}<br />
        @endforeach
    </div>
    @endif
    <h1 style="font-size: 30px;">Conferencias</h1>


    <table class="table table-striped table-bordered table-responsive" style="margin-top:15px">



        <thead>
            <th class="col-md-1">Nombre</th>
            <th class="col-md-1">Descripcion</th>
            <th class="col-md-1">Fecha</th>
            <th class="col-md-1">Hora de inicio</th>
            <th class="col-md-1">Hora de finalización</th>
            <th class="col-md-1">Cupo Disponible</th>
            <th class="col-md-1">Acciones</th>
        </thead>
        <tbody>
            @foreach($conferences as $conference)
            <tr>
                <td>
                    {{ $conference->name }}
                </td>
                <td>{{ $conference->description }}</td>
                <td>{{ $conference->date}}</td>
                <td>{{ $conference->start_at }}</td>
                <td>{{ $conference->end_at }}</td>
                <td>
                    {{ $conference->placesAvailable() }}
                </td>

                <td>
                    <ul>
                        <li>
                            <a href="{{ url('/proximas-conferencias\\'.$conference->id) }}" class="btn btn-success">Inscribirme</a>
                        </li>
                    </ul>
                </td>

            </tr>
            @endforeach


        </tbody>
    </table>

    {{$conferences->links()}}
</div>


@endsection