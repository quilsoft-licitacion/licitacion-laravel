@extends('layouts.app')

@section('content')



<div class="container">
    @if(Session::has('success'))
    <div class="alert alert-success" role="alert">
        {{Session::get('success')}}
    </div>

    @endif
    @if (\Session::has('error'))
    <div class="alert alert-danger">
        @foreach ($errors->all() as $error)
        {{ $error }}<br />
        @endforeach
    </div>
    @endif
    <h1 style="font-size: 30px;">Conferencias</h1>

    <div class="container">
        <div class="row">
            <div class="col-2 offset-5">
                @if (!is_null(Auth::user()) && Auth::user()->isAdmin())
                <a href="{{ url('/conferencias/create') }}" class="btn btn-success"> Crear Conferencia</a>
                @endif
            </div>
        </div>
    </div>
    <table class="table table-striped table-bordered table-responsive" style="margin-top:15px">



        <thead>
            <th class="col-md-1">Id</th>
            <th class="col-md-1">Nombre</th>
            <th class="col-md-1">Descripcion</th>
            <th class="col-md-1">Fecha</th>
            <th class="col-md-1">Hora de inicio</th>
            <th class="col-md-1">Hora de finalización</th>
            <th class="col-md-1">Cupo Disponible</th>
            <th class="col-md-1">Limite de horas para inscribirse</th>
            <th class="col-md-1">Acciones</th>
        </thead>
        <tbody>
            @foreach($conferences as $conference)
            <tr>
                <td> {{$conference->id}} </td>
                <td>
                    {{ $conference->name }}
                </td>
                <td>{{ $conference->description }}</td>
                <td>{{ $conference->date}}</td>
                <td>{{ $conference->start_at }}</td>
                <td>{{ $conference->end_at }}</td>
                <td>{{ $conference->places }}</td>
                <td>{{ $conference->limit_hours_to_enroll }}</td>
                <td>
                    <ul>
                        <li class="button-list">
                            <a href="{{ url('conferencias\\'.$conference->id) }}" class="btn btn-success"> Modificar</a>
                        </li>
                        <li class="button-list">
                            <form action="{{url('conferencias', [$conference->id])}}" method="POST">
                                {{method_field('DELETE')}}
                                {{csrf_field()}}
                                <input type="submit" class="btn btn-danger" value="Eliminar" />
                            </form>
                        </li>

                        <li class="button-list">
                            <a href="{{ url('conferencias\\'.$conference->id,'inscripciones') }}" class="btn btn-primary"> Inscripciones</a>
                        </li>

                    </ul>
                </td>

            </tr>
            @endforeach


        </tbody>
    </table>
    {{ $conferences->links() }}

</div>


@endsection