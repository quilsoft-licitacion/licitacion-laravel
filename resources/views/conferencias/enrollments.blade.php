@extends('layouts.app')

@section('content')



<div class="container">
    @if(Session::has('success'))
    <div class="alert alert-success" role="alert">
        {{Session::get('success')}}
    </div>

    @endif
    @if (\Session::has('error'))
    <div class="alert alert-danger">
        @foreach ($errors->all() as $error)
        {{ $error }}<br />
        @endforeach
    </div>
    @endif
    <h1 style="font-size: 30px;">Inscripciones al evento</h1>

    <table class="table table-striped table-bordered table-responsive" style="margin-top:15px">



        <thead>
            <th class="col-md-1">Estudiante</th>
            <th class="col-md-1">DNI</th>
            <th class="col-md-1">Acciones</th>
        </thead>
        <tbody>
            @foreach($enrollments as $enrollment)
            <tr>
                <td> {{$enrollment->user->first_name}} {{$enrollment->user->last_name}} </td>
                <td>
                    {{ $enrollment->user->dni }}
                </td>
                <td>
                    <form action="{{url('delete-enrollment', [$enrollment->id])}}" method="POST">
                        {{method_field('DELETE')}}
                        {{csrf_field()}}
                        <input type="submit" class="btn btn-danger" value="Eliminar" />
                    </form>
                </td>

            </tr>
            @endforeach


        </tbody>
    </table>
    {{ $enrollments->links() }}

</div>


@endsection