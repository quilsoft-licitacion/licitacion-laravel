@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            
            <form method="POST" action="{{ route('') }}">
                        @csrf
                        <div class="card">
                            <div class="card-header">Detalle del evento  :</div>
                            <p>{{ $enrollment->description }}</p>    
                            
                            
                            </div>
                        </div>            
                        

                <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            {{ __('Register') }}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection