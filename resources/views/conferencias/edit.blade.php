@extends('layouts.app')

@section('content')


<div class="container">
    @if(Session::has('success'))
    <div class="alert alert-success" role="alert">
        {{Session::get('success')}}
    </div>

    @endif
    @if (\Session::has('error'))
    <div class="alert alert-danger">
        @foreach ($errors->all() as $error)
        {{ $error }}<br />
        @endforeach
    </div>
    @endif

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Editar Conferencia') }}</div>

                <div class="card-body">
                    <form action="{{url('conferencias', [$conference->id])}}" method="POST">
                        {{ method_field('PATCH') }}
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nombre') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ $conference->name }}" required autofocus>

                            </div>
                        </div>

                        <div class=" form-group row">
                            <label for="descripcion" class="col-md-4 col-form-label text-md-right">{{ __('Descripcion') }}</label>
                            <textarea name="description" style="margin-left:10%;margin-right:10%;" class="form-control">{{ $conference->description }}</textarea>

                        </div>

                        <div class="form-group row">
                            <label for="cupos" class="col-md-4 col-form-label text-md-right">{{ __('Cupos del evento') }}</label>

                            <div class="col-md-6">
                                <input id="places" type="number" class="form-control" name="places" value="{{ $conference->places }}" required autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Fecha') }}</label>

                            <div class="col-md-6">
                                <input id="date" type="date" class="form-control" name="date" value="{{ $conference->date }}" required>

                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Inicio') }}</label>

                            <div class="col-md-6">
                                <input id="start_at" type="time" class="form-control" name="start_at" value="{{ $conference->start_at }}" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Fin') }}</label>

                            <div class="col-md-6">
                                <input id="end_at" type="time" class="form-control" name="end_at" value="{{ $conference->end_at }}" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="cupos" class="col-md-4 col-form-label text-md-right">{{ __('Limite de horas para la inscripcion') }}</label>

                            <div class="col-md-6">
                                <input id="limit_hours_to_enroll" type="number" class="form-control" name="limit_hours_to_enroll" value="{{ $conference->limit_hours_to_enroll }}" required autofocus>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Editar Conferencia') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection