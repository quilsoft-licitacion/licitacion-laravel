@extends('layouts.app')

@section('content')


<div class="container">
        <h1 style="font-size: 30px;">Conferencias</h1>
        <table class="table table-striped table-bordered">
            <thead>
                <th class="col-md-1">Id</th>
                <th class="col-md-1">Nombre</th>
                <th class="col-md-1">Descripcion</th>
                <th class="col-md-1">Fecha y Hora</th>
                <th class="col-md-1">Fecha Limite</th>
                <th class="col-md-1">Cupon Disponible</th>
                
            </thead>
            <tbody>
            @foreach($conferences as $conference)
                    <tr>
                        <td> {{$conference->id}} </td>
                        <td>
                            
                            {{ $conference->name }}
                        </td>
                        <td>{{ $conference->description }}</td>
                        <td>{{ $conference->date}}</td>
                        <td>{{ $conference->start_at }}</td>
                        <td>
                            {{ $conference->places }}
                            
                        </td>
                        
                        <td>
                            <ul>
                            <li class="button-list">
                                <a href="" class="btn btn-success">Inscribirme</a>
                            </li>
                                <!-- <li class="button-list">
                                    @if ($vehicle->featureds_count == 0)
                                        <a href="{{ url('admin\highlight\\'.$vehicle->id) }}" class="btn btn-success">Destacar</a>
                                    @else
                                        <a href="{{ url('admin\unhighlight\\'.$vehicle->id) }}" class="btn btn-danger"> Quitar de destacardos</a>
                                    @endif
                                </li>
                                <li class="button-list">
                                    @if ($vehicle->enabled == 0)
                                        <a href="{{ url('admin\enable\\'.$vehicle->id) }}" class="btn btn-success"> Habilitar</a>
                                    @else
                                        <a href="{{ url('admin\disable\\'.$vehicle->id) }}" class="btn btn-danger">Deshabilitar</a>
                                    @endif
                                </li> -->
                            </ul>
                        </td>

                    </tr>
                @endforeach
               

            </tbody>
        </table>
        
    </div>    


@endsection
