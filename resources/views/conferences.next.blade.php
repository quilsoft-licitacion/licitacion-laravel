@extends('layouts.app')


@section('content')

    <div class="container">
        <h1 style="font-size: 30px;">Conferencias</h1>
        <table class="table table-striped table-bordered">
            <thead>
                <th class="col-md-1">Id</th>
                <th class="col-md-1">Auto</th>
                <th class="col-md-1">Precio</th>
                <th class="col-md-1">Año</th>
                <th class="col-md-1">Kilometraje</th>
                <th class="col-md-1">Usuario</th>
                <th class="col-md-1">Acciones</th>

            </thead>


            <tbody>

                <!-- @foreach($vehicles as $vehicle)
                    <tr>
                        <td> {{$vehicle->id}} </td>
                        <td>
                            {{ $vehicle->submodel->model->brand->name  }}
                            {{ $vehicle->submodel->model->name }}
                            {{ $vehicle->submodel->name }}
                        </td>
                        {{-- Link que me lleve a detalles --}}
                        <td>{{ $vehicle->price }}</td>
                        <td>{{ $vehicle->year }}</td>
                        <td>{{ $vehicle->kilometers }}</td>
                        <td>
                            {{ $vehicle->user->first_name }}
                            {{ $vehicle->user->last_name }}
                            {{-- Link que me lleve a detalles --}}
                        </td>
                        <td>
                            <ul>
                                <li class="button-list">
                                    @if ($vehicle->featureds_count == 0)
                                        <a href="{{ url('admin\highlight\\'.$vehicle->id) }}" class="btn btn-success">Destacar</a>
                                    @else
                                        <a href="{{ url('admin\unhighlight\\'.$vehicle->id) }}" class="btn btn-danger"> Quitar de destacardos</a>
                                    @endif
                                </li>
                                <li class="button-list">
                                    @if ($vehicle->enabled == 0)
                                        <a href="{{ url('admin\enable\\'.$vehicle->id) }}" class="btn btn-success"> Habilitar</a>
                                    @else
                                        <a href="{{ url('admin\disable\\'.$vehicle->id) }}" class="btn btn-danger">Deshabilitar</a>
                                    @endif
                                </li>
                            </ul>
                        </td>

                    </tr>
                @endforeach -->

            </tbody>
        </table>
        <!-- {{ $vehicles->links() }} -->
    </div>
@endsection
